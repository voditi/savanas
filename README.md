# Alpha Framework

Alpha Framework é um conjunto de soluções CSS que auxiliam desenvolvedores novatos a se habituar com frameworks. O framework ainda está em fase de desenvolvimento, ajudas serão bem-vindas!

Requisitos:
 - NodeJS

# Como instalar:

1. Clone o repositório
2. Acesse a pasta via terminal
3. Digite: npm i
4. Digite: gulp
5. Enjoy!
