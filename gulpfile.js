const gulp          = require("gulp");
const pug           = require("gulp-pug");
const sass          = require("gulp-sass");
const notify        = require("gulp-notify");
const cssmin        = require("gulp-clean-css");
const browserSync   = require('browser-sync').create();

//////////////////////

var files = [
  'assets/scss/**/*.scss',
  'assets/js/*.js'
];

var options = {
  server: "public/"
};

///////////////////////


// Compile SASS Files
gulp.task("compile-sass", function () {
	return gulp.src("assets/scss/style.scss")
			.pipe(sass())
			.on("error", notify.onError({title:"Error at compile CSS", message:"<%= error.message %>"}))
			.pipe(gulp.dest("assets/css"))
            .pipe(browserSync.stream());
});


// Copy new fonts to the BUILD
gulp.task("copy-fonts", function() {
    gulp.src(["assets/scss/modules/font-awesome/fonts/*","assets/fonts/*"])
        .on("error", notify.onError({title:"Error at Copy Fonts", message:"<%= error.message %>"}))
        .pipe(gulp.dest("public/css/fonts"));
});


// Minify CSS Files
gulp.task("minify-css", function(){
  return gulp.src("assets/css/style.css")
    		.pipe(cssmin({compatibility: "ie8"}))
            .on("error", notify.onError({title:"Error at minify CSS", message:"<%= error.message %>"}))
   			.pipe(gulp.dest("public/css"));
});

// Minify Js
gulp.task("compile-js", function() {
    gulp.src("assets/js/*")
        .on("error", notify.onError({title:"Error at Compile Javascript", message:"<%= error.message %>"}))
        .pipe(gulp.dest("public/js"));
});

gulp.task("watch", function() {
    gulp.watch("assets/scss/**/*",      ["compile-sass"]);
    gulp.watch("assets/fonts/*",        ["copy-fonts"]);
    gulp.watch("assets/css/style.css",  ["minify-css"]);
    gulp.watch("assets/js/*",           ["compile-js"]);
});

// Browser reloading
gulp.task('server', ['compile-sass'], function() {

    browserSync.init(files, options)
    gulp.watch("assets/sass/*.scss", ['compile-sass']);
    gulp.watch("public/index.html",  ['compile-sass']);
    gulp.watch(files).on('change', browserSync.reload);
});

gulp.task("default",["compile-sass","copy-fonts","minify-css","compile-js","watch","server"]);
