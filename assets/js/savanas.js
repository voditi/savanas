// SOFT SCROLL
// ------------------------------

$(document).ready(function() {
  function filterPath(string) {
    return string
      .replace(/^\//,'')
      .replace(/(index|default).[a-zA-Z]{3,4}$/,'')
      .replace(/\/$/,'');
  }
  $('a[href*=#]').each(function() {
    if ( filterPath(location.pathname) == filterPath(this.pathname)
    && location.hostname == this.hostname
    && this.hash.replace(/#/,'') ) {
      var $targetId = $(this.hash), $targetAnchor = $('[name=' + this.hash.slice(1) +']');
      var $target = $targetId.length ? $targetId : $targetAnchor.length ? $targetAnchor : false;
       if ($target) {
         var targetOffset = $target.offset().top;
         $(this).click(function() {
           $('html, body').animate({scrollTop: targetOffset}, 650);
           return false;
         });
      }
    }
  });
});


$(document).ready(function() {

  // MENU OFF-CANVAS
  // ------------------------------

  $("button.drawer").click(function() {
   $(".drawer-menu").toggleClass("hide-drawer");
   $("body").toggleClass("overlay");
  });

  // MENU OFF-CANVAS
  // ------------------------------

  $('label[for^="modal"]').click(function () {
    $("body").toggleClass("overlay");
  });

  // TABS
  // ------------------------------

  $('.tab-item a').click(function(){
    var a = $(this);
    var active_tab_class = 'active';
    var the_tab = '.' + a.attr('data-tab');

    $('.tab-item a').removeClass(active_tab_class);
    a.addClass(active_tab_class);

    $('.tabs-content .tabs').css({
      'display' : 'none'
    });

    $(the_tab).show();

    return false;
  });

  $(window).bind('scroll', function () {

    if ($(window).scrollTop() > 300) {
      $('.navbar').addClass('animate slideDown fixed').removeClass('slideUp');
    } else {
      $('.navbar').addClass('slideUp').removeClass('slideDown');

      if ($(window).scrollTop() < 100) {
        $('.navbar').removeClass('slideUp fixed');
      }
    }

  });
});